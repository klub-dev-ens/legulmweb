# Settings

The template settings for the project are in `settings.{dev,prod}.py`. Before
running the app, copy the relevant file as `settings.py`, edit it and fix
everything that must be.

You should not have to edit `settings_base.py`; however, if you need to alter
some setting from there, override it directly in `settings.py`.

""" Base settings for the project, that you probably won't need to modify

Those can be overriden nevertheless by re-defining it in `settings.py`
"""

import os
from django.contrib import messages


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PUBLIC_DIR = os.path.join(BASE_DIR, "public")


# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "extra_views",
    "user_profile",
    "compta",
    "permanences",
    "mainsite",
    "widget_tweaks",  # For allauth
    "allauth_ens",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "allauth_cas",
    "allauth_ens.providers.clipper",
    "solo",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "legulmweb.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "legulmweb.wsgi.application"


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {"console": {"class": "logging.StreamHandler"}},
    "loggers": {"legulmweb": {"handlers": ["console"], "level": "INFO"}},
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

SOCIALACCOUNT_PROVIDERS = {
    "clipper": {
        # These settings control whether a message containing a link to
        # disconnect from the CAS server is added when users log out.
        "MESSAGE_SUGGEST_LOGOUT_ON_LOGOUT": True,
        "MESSAGE_SUGGEST_LOGOUT_ON_LOGOUT_LEVEL": messages.INFO,
    }
}
SOCIALACCOUNT_ADAPTER = "allauth_ens.adapter.LongTermClipperAccountAdapter"

SITE_ID = 1

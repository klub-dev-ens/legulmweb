"""legulmweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from allauth_ens.views import capture_login, capture_logout
import allauth_ens.urls

import mainsite.urls
import permanences.urls
import user_profile.urls

urlpatterns = [
    path("admin/login/", capture_login),
    path("admin/logout/", capture_logout),
    path("accounts/", include(allauth_ens.urls)),
    path("accounts/", include(user_profile.urls)),
    path("admin/", admin.site.urls),
    path("permanences/", include(permanences.urls)),
    path("", include(mainsite.urls)),
]

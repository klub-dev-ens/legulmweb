from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
import datetime
from solo.models import SingletonModel

from compta.models import Delivery


class PermanencesConfig(SingletonModel):
    """ Permanences configuration as a model and  admin view """

    permanences_isoweekday = models.IntegerField(
        choices=(
            ("1", "Lundi"),
            ("2", "Mardi"),
            ("3", "Mercredi"),
            ("4", "Jeudi"),
            ("5", "Vendredi"),
            ("6", "Samedi"),
            ("7", "Dimanche"),
        ),
        default=2,
        verbose_name="Jour de distribution",
    )

    start_time = models.TimeField(
        verbose_name="Début de permanence", default=datetime.time(hour=18, minute=0)
    )
    end_time = models.TimeField(
        verbose_name="Fin de permanence", default=datetime.time(hour=19, minute=45)
    )
    slot_duration = models.IntegerField(
        verbose_name="Durée de créneau",
        default=30,
        help_text=(
            "En minutes. On complète à la fin avec un créneau plus court si besoin."
        ),
    )
    shown_ahead_perms = models.IntegerField(
        verbose_name="Nombre de permanences dans le futur affichées", default=8
    )

    first_subscribe_call_days = models.IntegerField(
        verbose_name="Premier rappel d'inscription",
        help_text=(
            "Un premier appel à l'inscription en permanence est envoyé tant de "
            "jours avant la permanence à toutes les personnes n'ayant pas tenu de "
            "permanence cette année s'il manque des volontaires. "
            "Seuls les membres ayant pris un panier à la distribution concernée ou la "
            "précédente seront contactés. "
        ),
        default=6,
    )
    second_subscribe_call_days = models.IntegerField(
        verbose_name="Second rappel d'inscription",
        help_text=(
            "Un second appel à l'inscription en permanence est envoyé tant de "
            "jours avant la permanence à toutes les personnes ayant tenu moins de "
            "X permanences cette année (X plus bas) s'il manque des volontaires. "
            "Seuls les membres ayant pris un panier à la distribution concernée ou la "
            "précédente seront contactés. "
        ),
        default=3,
    )
    second_subscribe_call_threshold = models.IntegerField(
        verbose_name="Second rappel d'inscription si moins de",
        help_text=(
            "Le second appel sera envoyé à celleux ayant tenu moins te tant de "
            "permanences cette année (inclusif)."
        ),
        default=2,
    )
    volunteer_remainder_days = models.IntegerField(
        verbose_name="Rappel aux volontaires (jours)",
        help_text=(
            "Un mail de rappel est envoyé aux volontaires tant de jours avant leur "
            "permanence."
        ),
        default=2,
    )
    last_subscribe_call_days = models.IntegerField(
        verbose_name="Dernier rappel d'inscription",
        help_text=(
            "Un dernier appel à l'inscription en permanence est envoyé tant de"
            "jours avant la permanence à tout le monde s'il manque encore des "
            "volontaires."
            "Seuls les membres ayant pris un panier à la distribution concernée ou la "
            "précédente seront contactés. "
        ),
        default=1,
    )

    def clean(self):
        """ Checks various properties """
        if self.first_subscribe_call_days <= self.second_subscribe_call_days:
            raise ValidationError("Le premier rappel doit avoir lieu avant le second")
        if self.second_subscribe_call_days <= self.last_subscribe_call_days:
            raise ValidationError("Le second rappel doit avoir lieu avant le dernier")
        if self.volunteer_remainder_days <= self.last_subscribe_call_days:
            raise ValidationError(
                "Le mail de rappel aux volontaires doit être envoyé "
                "avant le dernier rappel d'inscription."
            )


class PermanenceDate(models.Model):
    """ A date on which a delivery will take place, and volunteers will have to be
    present """

    delivery = models.OneToOneField(
        Delivery, on_delete=models.CASCADE, related_name="permanence"
    )

    first_call = models.BooleanField(
        default=False, verbose_name="Premier appel effectué"
    )
    second_call = models.BooleanField(
        default=False, verbose_name="Second appel effectué"
    )
    last_call = models.BooleanField(
        default=False, verbose_name="Dernier appel effectué"
    )
    volunteer_remainder_sent = models.BooleanField(
        default=False, verbose_name="Rappel aux volontaires effectué"
    )

    @property
    def date(self):
        return self.delivery.date

    def days_from_now(self):
        """ Returns a timedelta from today, in days """
        return (self.date - timezone.localdate()).days

    @classmethod
    def generate_upcoming(cls, nb_upcoming):
        """ Ensures that at least `nb_upcoming` permanences are upcoming. If there is
        not enough upcoming permanences, generates some according to the
        settings-defined rules; if there is already enough, does nothing. """

        perm_config = PermanencesConfig.get_solo()

        upcoming = cls.objects.filter(delivery__date__gte=timezone.localdate())[
            :nb_upcoming
        ].count()
        if upcoming < nb_upcoming:
            missing_nb = nb_upcoming - upcoming
            start_at_date = timezone.localdate()
            try:
                last_perm = cls.objects.order_by("-delivery__date")[0]
                start_at_date = max(start_at_date, last_perm.date)
            except IndexError:  # No perm in DB yet
                pass
            days_to_next_perm = (
                perm_config.permanences_isoweekday - start_at_date.isoweekday()
            ) % 7
            cur_gen_date = start_at_date + datetime.timedelta(days=days_to_next_perm)
            for _ in range(missing_nb):
                delivery = Delivery(date=cur_gen_date)
                delivery.full_clean()
                delivery.save()

                perm_date = cls(delivery=delivery)
                perm_date.full_clean()
                perm_date.save()
                PermanenceSlot.gen_default_slots(perm_date)
                cur_gen_date += datetime.timedelta(weeks=1)

    @classmethod
    def get_upcoming_perms(cls, limit):
        """ Get the `limit` upcoming permmanence objects, creating some if necessary
        according to the website's settings """

        cls.generate_upcoming(limit)
        return cls.objects.filter(delivery__date__gte=timezone.localdate())[:limit]


class PermanenceSlot(models.Model):
    """ A time slot during a `PermanenceDate` """

    permanence = models.ForeignKey(
        PermanenceDate, on_delete=models.CASCADE, related_name="slots"
    )

    # Here we use DateTime instead of Time fields, because TimeField cannot be
    # timezone-aware. A validation step below ensures those dates are on the
    # permanence's day.
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    volunteer = models.ForeignKey(
        "user_profile.Profile", on_delete=models.CASCADE, null=True, blank=True
    )

    def filled_status(self, req):
        """ Returns the "filled status" of the slot: free, taken by the user or taken
        by some other user; respectively `free`, `filled_self` or `filled` """

        if not self.volunteer:
            return "free"
        if req.user.is_authenticated and req.user.profile == self.volunteer:
            return "filled_self"
        return "filled"

    @classmethod
    def gen_default_slots(cls, perm_date):
        """ Generates the default slots for the given PermanenceDate """
        perm_config = PermanencesConfig.get_solo()
        cur_time = timezone.make_aware(
            datetime.datetime.combine(perm_date.date, perm_config.start_time)
        )
        end_time = timezone.make_aware(
            datetime.datetime.combine(perm_date.date, perm_config.end_time)
        )
        while cur_time < end_time:
            next_time = cur_time + datetime.timedelta(minutes=perm_config.slot_duration)
            if next_time > end_time:
                next_time = end_time
            cur_slot = cls(
                permanence=perm_date,
                start_date=cur_time,
                end_date=next_time,
                volunteer=None,
            )
            cur_slot.full_clean()
            cur_slot.save()
            cur_time = next_time

    def clean(self):
        """ Checks that `start_date` and `end_date` are actually on `permanence`'s day
        """
        super().clean()
        perm_date = self.permanence.date
        if perm_date != self.start_date.date():
            raise ValidationError(
                (
                    "The start date of this object ({}) is not on the same day as its "
                    "related permanence ({})."
                ).format(self.start_date.date(), perm_date)
            )
        if perm_date != self.end_date.date():
            raise ValidationError(
                (
                    "The end date of this object ({}) is not on the same day as its "
                    "related permanence ({})."
                ).format(self.start_date.date(), perm_date)
            )

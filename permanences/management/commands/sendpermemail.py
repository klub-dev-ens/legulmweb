from django.core.management.base import BaseCommand, CommandError
from django.core import mail
from permanences import emails


class Command(BaseCommand):
    help = "Send automatic scheduled emails for permanences"

    def handle(self, *args, **kwargs):
        emails.do_permanence_remainders(self)
        emails.do_email_calls(self)

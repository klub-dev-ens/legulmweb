from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.views.generic import TemplateView
from django.utils import timezone
from django.utils.safestring import SafeString
from django.template.loader import render_to_string
import datetime
from .models import PermanenceDate, PermanenceSlot, PermanencesConfig
from . import emails


class InvalidFormException(Exception):
    """ Raised when HTML form data fails to validate correctly (for non-django forms)
    """

    pass


class PermTableView(TemplateView):
    """ A view of the registration table for upcoming permanences """

    template_name = "permanences/perm_table.html"

    def get_context_data(self, **kwargs):
        """ We need to fetch the upcoming permamences for the view """
        perm_config = PermanencesConfig.get_solo()
        upcoming_perms = PermanenceDate.get_upcoming_perms(
            limit=perm_config.shown_ahead_perms
        )

        context = super().get_context_data(**kwargs)

        perms = []
        for perm_obj in upcoming_perms:
            slot_objs = perm_obj.slots.all()
            template_slots = []
            for slot_obj in slot_objs:
                template_slots.append(
                    {
                        "obj": slot_obj,
                        "filled_status": slot_obj.filled_status(self.request),
                    }
                )

            perms.append({"obj": perm_obj, "slots": template_slots})

        context["perms"] = perms

        if self.request.user.is_authenticated:
            context[
                "perms_taken_this_year"
            ] = self.request.user.profile.permanences_this_year_count()
            context[
                "future_perms_number"
            ] = self.request.user.profile.permanences_upcoming_count()
        return context

    def do_render(self):
        """ Renders the actual page """
        context = self.get_context_data()
        return self.render_to_response(context)

    def get(self, req, *args, **kwargs):
        """ Handles GET requests such as unregistering from a slot, as well as basic
        template rendering. """

        data = req.GET

        if "unregister" in data:  # Unregistering from a slot
            try:
                slot = PermanenceSlot.objects.get(pk=data["unregister"])
                if not req.user.is_authenticated or (
                    slot.volunteer != req.user.profile and not req.user.is_staff
                ):
                    raise PermissionDenied
            except (ObjectDoesNotExist, PermanenceSlot.MultipleObjectsReturned):
                raise PermissionDenied

            perm_config = PermanencesConfig.get_solo()
            if (
                not req.user.is_staff
                and (slot.permanence.date - timezone.localdate()).days
                <= perm_config.last_subscribe_call_days
            ):
                messages.warning(
                    req,
                    (
                        "Cette permanence est dans trop peu de temps ! Pour vous en "
                        "désinscrire, veuillez contacter l'équipe Légulm."
                    ),
                )
                # TODO lien de contact
            else:
                slot.volunteer = None
                slot.full_clean()
                slot.save()

                messages.success(
                    req,
                    "Vous vous êtes désinscrit·e de votre permanence du {}.".format(
                        slot.permanence.date
                    ),
                )
        return super().get(req, *args, **kwargs)

    def post(self, req, *args, **kwargs):
        """ Handles registration through the form.

        An actual Django form here seems to be more a pain than a real perk, so we
        implement it by hand (the form does not correspond to an actual model, changes
        according to the page render date, ...) """

        data = req.POST
        checked_fields = []

        try:
            if not req.user.is_authenticated:  # User must be logged in
                messages.warning(
                    req, "Vous devez être connecté·e pour prendre une permanence."
                )
                raise InvalidFormException()

            for field_pk in data:
                if not field_pk.startswith("slot_"):  # Not a slot <input>
                    continue
                pk = field_pk[5:]
                if not data[field_pk]:  # Only consider actually checked fields
                    continue
                matching_pk = PermanenceSlot.objects.filter(pk=pk)
                if matching_pk.count() != 1:  # There should be exactly one
                    messages.warning(
                        req,
                        (
                            "Au moins un créneau sélectionné est invalide. Si le "
                            "problème persiste, veuillez contacter les "
                            "administrateur·rice·s du site web"
                        ),
                    )
                    raise InvalidFormException()
                else:
                    perm_slot = matching_pk[0]
                    if perm_slot.start_date < timezone.now():
                        messages.warning(
                            req, "Au moins un créneau sélectionné est dans le passé."
                        )
                        raise InvalidFormException()
                    if perm_slot.volunteer:  # Slot is already taken
                        messages.warning(
                            req,
                            (
                                "Au moins un créneau sélectionné est déjà pris par "
                                "un·e autre volontaire."
                            ),
                        )
                        raise InvalidFormException()
                    checked_fields.append(perm_slot)

            if not checked_fields:  # No checked fields at all
                messages.warning(req, "Aucun créneau n'a été sélectionné.")
                raise InvalidFormException()

        except InvalidFormException:  # Invalid form
            return self.do_render()

        for perm_slot in checked_fields:
            perm_slot.volunteer = req.user.profile
            perm_slot.full_clean()
        for perm_slot in checked_fields:  # Fully validate, then commit
            perm_slot.save()

        messages.success(
            req,
            SafeString(
                render_to_string(
                    "permanences/messages/perm_taken.html",
                    context={"perms": checked_fields},
                )
            ),
        )
        emails.upon_perm_sign_up(self.request.user)
        return self.do_render()

from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import PermanencesConfig

admin.site.register(PermanencesConfig, SingletonModelAdmin)

""" Handles emails sent regarding permanence slots """

from django.utils import timezone
from django.template.loader import render_to_string
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
from django.contrib.auth.models import User as DjangoUser
from .models import PermanencesConfig, PermanenceDate, PermanenceSlot
from compta.models import Delivery

from smtplib import SMTPException


def do_send_mail(subject, body, member):
    """ Wrapper for mail sending """

    subject = "[Légulm] " + subject
    message = EmailMessage(
        subject=subject,
        body=body,
        from_email=settings.FROM_EMAIL,
        to=[member.email],
        reply_to=[settings.CONTACT_EMAIL],
        headers={"List-Id": "<legulm>"},
    )
    try:
        message.send()
        return True
    except SMTPException:
        return False  # TODO log errors


def do_send_mail_cmd(subject, body, member, cmd_obj):
    """ Wrapper around `do_send_email` for when called from within a management command
    """

    if not do_send_mail(subject, body, member):
        if cmd_obj:
            cmd_obj.stdout.write(
                cmd_obj.style.NOTICE(
                    "FAILED sending mail to {} <{}>".format(member, member.email)
                )
            )


class PermanenceCall:
    """ A permanence call: sends an email to every member having baskets in either this
    delivery or the previous one, who did fewer or equal permanences than
    `perms_threshold`, in case we are currently `days_before` days before the delivery,
    checking whether the mails were sent with the attribute `call_sent_attr`.
    """

    def __init__(self, perms_threshold, days_before, call_sent_attr, cmd_obj=None):
        self.perms_threshold = perms_threshold
        self.days_before = days_before
        self.call_sent_attr = call_sent_attr
        self.cmd_obj = cmd_obj

    def send_call(self, perm, members):
        """ Actually sends the call to these members """
        next_perm = PermanenceDate.objects.filter(
            delivery__date__gte=timezone.now()
        ).order_by("delivery__date")[0]
        is_next_perm = next_perm == perm
        free_slots = perm.slots.filter(volunteer=None)
        nb_free_slots = free_slots.count()

        for member in members:
            context = {
                "full_name": member,
                "nb_free_slots": nb_free_slots,
                "next_delivery": is_next_perm,
                "date": perm.date,
                "slots": free_slots,
            }
            subject = "{} : manque des volontaires !".format(
                perm.date.strftime("%d/%m/%y")
            )
            body = render_to_string(
                "permanences/email/permanence_call.txt", context=context
            )
            do_send_mail_cmd(subject, body, member, self.cmd_obj)

    def call(self, perm):
        """ Actually runs the call """
        if perm.__dict__[self.call_sent_attr]:
            return  # Call already passed

        if (perm.date - timezone.localdate()).days > self.days_before:
            return  # Too early for this call

        members_implied = perm.delivery.users_implied()
        try:
            prev_delivery = perm.delivery.get_previous_by_date()
            previously_implied = prev_delivery.users_implied()
            members_implied = members_implied.union(previously_implied)
        except Delivery.DoesNotExist:
            pass

        calling_members = set()
        for member in members_implied:
            perms_count = member.profile.permanences_this_year_count(
                include_upcoming=True
            )
            if self.perms_threshold < 0 or perms_count <= self.perms_threshold:
                calling_members.add(member)

        self.send_call(perm, calling_members)

        perm.__dict__[self.call_sent_attr] = True
        perm.save()


def get_upcoming_perms(max_days):
    """ Retrieves a list of upcoming permanences that might be within `max_days` """
    return PermanenceDate.get_upcoming_perms(limit=(max_days + 6) // 7)


def do_email_calls(cmd_obj):
    """ Sends permanence sign-up calls """
    perm_conf = PermanencesConfig.get_solo()
    upcoming_perms = get_upcoming_perms(perm_conf.first_subscribe_call_days)

    calls_settings = [
        PermanenceCall(
            perms_threshold=0,
            days_before=perm_conf.first_subscribe_call_days,
            call_sent_attr="first_call",
            cmd_obj=cmd_obj,
        ),
        PermanenceCall(
            perms_threshold=perm_conf.second_subscribe_call_threshold,
            days_before=perm_conf.second_subscribe_call_days,
            call_sent_attr="second_call",
            cmd_obj=cmd_obj,
        ),
        PermanenceCall(
            perms_threshold=-1,
            days_before=perm_conf.last_subscribe_call_days,
            call_sent_attr="last_call",
            cmd_obj=cmd_obj,
        ),
    ]

    for call in calls_settings:
        for perm in upcoming_perms:
            call.call(perm)


def upon_perm_sign_up(user):
    """ Sends an email to confirm their permanences sign up to the user """

    if not user.profile.receive_perm_subscribe_email:
        return  # This user doesn't want emails on subscription

    context = {
        "full_name": user,
        "slots": user.profile.permanences_upcoming(),
        "mail_settings_url": user.profile.direct_email_settings_url,
    }

    subject = "Inscription en permanences"
    body = render_to_string("permanences/email/permanence_signup.txt", context=context)
    do_send_mail(subject, body, user)


def do_permanence_remainders(cmd_obj):
    """ Sends remainder emails to the members signed up for a permanence coming soon
    """

    def send_remainder(perm, volunteer):
        """ Actually sends the remainder to one user """
        context = {
            "full_name": volunteer.user,
            "date": perm.date,
            "slots": [],
            "first_slot": perm.slots.first().volunteer == volunteer,
            "last_slot": perm.slots.last().volunteer == volunteer,
        }
        for slot in perm.slots.all():
            if slot.volunteer == volunteer:
                context["slots"].append(slot)

        subject = "Rappel : permanence du {}".format(perm.date.strftime("%d/%m/%y"))
        body = render_to_string(
            "permanences/email/permanence_remainder.txt", context=context
        )
        do_send_mail_cmd(subject, body, volunteer.user, cmd_obj)

    perm_conf = PermanencesConfig.get_solo()
    upcoming_perms = get_upcoming_perms(perm_conf.volunteer_remainder_days)
    for perm in upcoming_perms:
        if perm.volunteer_remainder_sent:
            continue  # Already sent
        if (perm.date - timezone.localdate()).days > perm_conf.volunteer_remainder_days:
            continue  # Not yet

        must_remind = set()
        for slot in perm.slots.all():
            volunteer = slot.volunteer
            if not volunteer:
                continue  # No volunteer
            must_remind.add(volunteer)

        for volunteer in must_remind:
            send_remainder(perm, volunteer)

        perm.volunteer_remainder_sent = True
        perm.save()

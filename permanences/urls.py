from django.urls import path
from .views import PermTableView

app_name = "permanences"

urlpatterns = [path("", PermTableView.as_view(), name="table")]

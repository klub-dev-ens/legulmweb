from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.views.generic.base import TemplateView, ContextMixin
from django.views.generic.edit import UpdateView
from django.urls import reverse
from .models import Profile, DjangoUser


class DjangoUserForm(forms.ModelForm):
    """ DjangoUser part of the user settings """

    first_name = forms.CharField(max_length=30, required=True, label="Prénom")
    last_name = forms.CharField(max_length=150, required=True, label="Nom")
    email = forms.EmailField(required=True, label="Email")

    class Meta:
        model = DjangoUser
        fields = ["first_name", "last_name", "email"]


class ProfileForm(forms.ModelForm):
    """ Profile part of the user settings """

    class Meta:
        model = Profile
        fields = ["receive_perm_subscribe_email"]


class UserSettingsView(LoginRequiredMixin, TemplateView):
    """ User settings view, from where they might change their preferences and such """

    template_name = "user_profile/user_settings.html"

    def get_forms(self):
        """ Constructs a (DjangoUserForm, ProfileForm) pair filled with current data
        upon GET or submitted data upon POST """
        if self.request.method == "GET":
            return (
                DjangoUserForm(instance=self.object),
                ProfileForm(instance=self.object.profile),
            )
        if self.request.method == "POST":
            return (
                DjangoUserForm(self.request.POST, instance=self.object),
                ProfileForm(self.request.POST, instance=self.object.profile),
            )
        return None  # Will yield an HttpMethodNotAllowed somewhere anyway

    def get(self, request, *args, **kwargs):
        self.object = request.user
        form, profile_form = self.get_forms()
        context = self.get_context_data(form=form, profile_form=profile_form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        self.object = request.user
        form, profile_form = self.get_forms()
        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        return self.form_invalid(form, profile_form)

    def form_valid(self, form, profile_form):
        """ Called upon POST request when data is valid """
        form.save()
        profile_form.save()
        messages.info(self.request, "Profil modifié avec succès !")
        return self.render_to_response(
            context=self.get_context_data(form=form, profile_form=profile_form)
        )

    def form_invalid(self, form, profile_form):
        """ Called upon POST request when data is NOT valid """
        messages.error(self.request, "Veuillez corriger les erreurs ci-dessous.")
        return self.render_to_response(
            self.get_context_data(form=form, profile_form=profile_form)
        )


class EmailSettingsForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["receive_perm_subscribe_email"]


class UserEmailSettingsView(UpdateView):
    """ Login-bypassing view to modify email settings """

    model = Profile
    fields = ["receive_perm_subscribe_email"]
    slug_field = "email_settings_token"
    slug_url_kwarg = "token"
    template_name = "user_profile/email_settings.html"

    def get_context_data(self, **kwargs):
        base_context = super().get_context_data(**kwargs)
        base_context["user"] = self.object.user
        return base_context

    def form_valid(self, form):
        form.save()
        messages.info(self.request, "Préférences email mises à jour")
        return self.render_to_response(self.get_context_data())

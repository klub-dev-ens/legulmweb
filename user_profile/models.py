""" User Profile

Extends the original Django user model with additional data """

import datetime
import uuid

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User as DjangoUser
from django.contrib.sites.models import Site
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone

from compta.models import Payment, Order
from permanences.models import PermanenceSlot


class Profile(models.Model):
    """ Extend Django's user profile """

    user = models.OneToOneField(DjangoUser, on_delete=models.CASCADE)

    receive_perm_subscribe_email = models.BooleanField(
        verbose_name="Recevoir des mails à l'inscription en permanence",
        help_text=(
            "Un email sera envoyé lorsque vous vous inscrivez à une permanence, "
            "pour vous servir de rappel. Un autre sera envoyé dans tous les "
            "cas peu de temps avant la permanence."
        ),
        default=True,
    )

    email_settings_token = models.UUIDField(default=uuid.uuid4, unique=True)

    def __str__(self):
        return str(self.user)

    def balance(self):
        """ Get the user's balance """
        payments = Payment.objects.filter(user=self.user)
        orders = Order.objects.filter(user=self.user)

        cur_balance = 0
        cur_balance += sum(map(lambda x: x.value, payments))
        cur_balance -= sum(map(lambda x: x.baskets_number * Order.BASKET_PRICE, orders))

        return cur_balance

    def order_for(self, delivery):
        """ The number of baskets ordered for this delivery """
        matching_orders = Order.objects.filter(user=self.user, delivery=delivery)
        num_baskets = sum(lambda x: x.baskets_number, matching_orders)
        return num_baskets

    def permanences_total_count(self):
        """ Total number of permanences this user did ever """
        return PermanenceSlot.objects.filter(
            volunteer=self, permanence__delivery__date__lt=timezone.localdate()
        ).count()

    def permanences_this_year_count(self, include_upcoming=False):
        """ The number of permanences the user did this year. This count will include
        the permanences the user signed up for but did not already do only if
        `include_upcoming` is `True`. """

        def get_year_start():
            """ Returns a datetime.date object for the first day of the current scholar
            year (ie. September 1st). This day is always in the past. """

            today = timezone.localdate()
            if today.month < 9:  # Before september
                return datetime.date(year=today.year - 1, month=9, day=1)
            return datetime.date(year=today.year, month=9, day=1)

        year_start = get_year_start()

        if include_upcoming:
            return PermanenceSlot.objects.filter(
                volunteer=self, permanence__delivery__date__gte=year_start
            ).count()
        return PermanenceSlot.objects.filter(
            volunteer=self,
            permanence__delivery__date__gte=year_start,
            permanence__delivery__date__lt=timezone.localdate(),
        ).count()

    def permanences_upcoming(self):
        """ Upcoming permanences this user signed up for """
        return PermanenceSlot.objects.filter(
            volunteer=self, permanence__delivery__date__gte=timezone.localdate()
        )

    def permanences_upcoming_count(self):
        """ Number of upcoming permanences this user signed up for """
        return self.permanences_upcoming().count()

    @property
    def direct_email_settings_url(self):
        site_url = Site.objects.get_current().domain
        page_url = reverse(
            "user_profile:email_settings_view",
            kwargs={"token": self.email_settings_token},
        )
        return "https://{}{}".format(site_url, page_url)

    @staticmethod
    def create_for(user):
        """ Create a Profile for a newly created DjangoUser """
        profile = Profile(user=user)
        profile.full_clean()
        profile.save()


@receiver(post_save, sender=DjangoUser)
def create_user_profile(sender, instance, created, **kwargs):
    """ Create automatically a Profile whenever a User is created """
    if created:
        Profile.create_for(user=instance)


@receiver(post_save, sender=DjangoUser)
def save_user_profile(sender, instance, **kwargs):
    """ Trigger Profile.save() upon User.save() """
    instance.profile.save()

from django.urls import path, register_converter
from .views import UserSettingsView, UserEmailSettingsView

app_name = "user_profile"

urlpatterns = [
    path("profile", UserSettingsView.as_view(), name="profile"),
    path(
        "email_settings/<uuid:token>",
        UserEmailSettingsView.as_view(),
        name="email_settings_view",
    ),
]

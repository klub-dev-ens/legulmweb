# Légulm Web

Le site web de Légulm, l'AMAP de l'ENS. Sert en particulier à toute la
comptabilité de l'asso.

## Installation

En plus de l'installation standard d'un site Django, il faudra penser à lancer
périodiquement les commandes suivantes.

* Tous les jours, `./manage.py sendpermemail`
* Dans les premiers jours de novembre chaque année,
  `./manage.py deprecate_clippers`
  ([allauth-ens](https://pypi.org/project/django-allauth-ens/))

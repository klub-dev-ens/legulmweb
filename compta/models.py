""" Models for compta uses """

from django.db import models
from django.db.utils import NotSupportedError
from django.contrib.auth.models import User as DjangoUser

from .utils import ChoiceEnum


class Delivery(models.Model):
    """ An AMAP delivery date """

    date = models.DateField(help_text="Delivery's date")

    def users_implied(self):
        """ Returns the set of users having a basket during this delivery """
        out_set = set()
        try:
            orders = self.order_set.distinct("user").select_related("user").all()
            for order in orders:
                out_set.add(order.user)
        except NotSupportedError:
            orders = self.order_set.select_related("user").all()
            for order in orders:
                out_set.add(order.user)
        return out_set

    class Meta:
        verbose_name = "Delivery"
        verbose_name_plural = "Deliveries"


class Payment(models.Model):
    """ A payment entry, linked to a user """

    class PaymentMean(ChoiceEnum):
        """ Means of payment (check, cash, ...) """

        check = "CHQ"
        cash = "LIQ"

    value = models.IntegerField(help_text="The amount of money")
    user = models.ForeignKey(DjangoUser, on_delete=models.CASCADE)
    date = models.DateField(help_text="Date on which the payment was made")
    mean = models.CharField(
        max_length=3, choices=PaymentMean.choices(), help_text="Mean of payment used"
    )


class Order(models.Model):
    """ An order for baskets on a given date """

    BASKET_PRICE = 10  # Euros
    # WARNING: do not change `BASKET_PRICE`! If it is changed once in
    # production, this will break down balance values.
    # If it ever needs be changed, we will need to implement something smarter.

    delivery = models.ForeignKey(Delivery, on_delete=models.CASCADE)
    baskets_number = models.IntegerField(
        help_text="Number of baskets included in this order"
    )
    payment = models.ForeignKey(
        Payment,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text=(
            "The payment this order is linked to. If the order results from a "
            "positive account balance, this field might be null"
        ),
    )
    user = models.ForeignKey(
        DjangoUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        help_text=(
            "The user that passed this order. This field might be null if "
            "the user deleted their account."
        ),
    )

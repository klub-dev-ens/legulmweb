''' Various utilities used as tools in diverse contexts '''

from enum import Enum


class ChoiceEnum(Enum):
    ''' An enumeration to be used as a `choices` argument for Django fields '''
    @classmethod
    def choices(cls):
        ''' Turns the various options into `choices`-compatible structure '''
        return tuple((i.name, i.value) for i in cls)
